<?php

declare(strict_types=1);

namespace Cycle\Schema\Tests\Driver\SQLServer;

use Cycle\Schema\Tests\Relation\HasOneRelationTest as BaseTest;

class HasOneRelationTest extends BaseTest
{
    public const DRIVER = 'sqlserver';
}
