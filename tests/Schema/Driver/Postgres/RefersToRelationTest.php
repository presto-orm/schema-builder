<?php

declare(strict_types=1);

namespace Cycle\Schema\Tests\Driver\Postgres;

use Cycle\Schema\Tests\Relation\RefersToRelationTest as BaseTest;

class RefersToRelationTest extends BaseTest
{
    public const DRIVER = 'postgres';
}
