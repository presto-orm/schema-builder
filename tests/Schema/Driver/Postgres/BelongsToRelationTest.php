<?php

declare(strict_types=1);

namespace Cycle\Schema\Tests\Driver\Postgres;

use Cycle\Schema\Tests\Relation\BelongsToRelationTest as BaseTest;

class BelongsToRelationTest extends BaseTest
{
    public const DRIVER = 'postgres';
}
