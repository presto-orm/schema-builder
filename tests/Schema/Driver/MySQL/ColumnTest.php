<?php

declare(strict_types=1);

namespace Cycle\Schema\Tests\Driver\MySQL;

use Cycle\Schema\Tests\ColumnTest as BaseTest;

class ColumnTest extends BaseTest
{
    public const DRIVER = 'mysql';
}
